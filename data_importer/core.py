import openpyxl

from collections import defaultdict, OrderedDict

from datetime import datetime

from django.core.exceptions import ValidationError

from data_importer.models import FileUploadHistory


class DataImporter:
    columns = tuple()
    first_valid_line = 1
    workbook = None

    def __init__(self, **kwargs):
        self.file_upload_history = kwargs.get('instance')
        self.cleaned_data = dict()
        self.errors = defaultdict(list)

    def execute(self):
        try:
            for row_number, row_values in self._read_excel_file():
                clean_data, with_error = self._validate_row(row_number, row_values)

                if not with_error:
                    clean_data = self.post_clean(clean_data)
                    self.save(clean_data)

            self.post_execute()
            self.file_upload_history.finish_execution = datetime.now()
            self.file_upload_history.state = FileUploadHistory.PROCESSED

            if self.errors:
                self.file_upload_history.errors = self.errors
        except Exception as e:
            self.file_upload_history.finish_execution = datetime.now()
            self.file_upload_history.state = FileUploadHistory.FAILED
            self.file_upload_history.errors = {'error': '{0}'.format(e)}

        self.file_upload_history.save()

    def _read_excel_file(self):
        file_path = self.file_upload_history.file_uploaded.path
        workbook = openpyxl.load_workbook(filename=file_path)

        sheet = workbook.active
        for row_number, row in enumerate(sheet.iter_rows(), 1):
            row_values = OrderedDict()
            if self.first_valid_line != row_number:
                for field, index in self.columns:
                    row_values[field] = row[index].value
                yield row_number, row_values

    def _validate_row(self, row_number, row_values):
        clean_data = dict()
        with_error = False

        for field, value in row_values.items():
            validator = 'clean_{0}'.format(field)

            if hasattr(self, validator):
                try:
                    clean_data[field] = getattr(self, validator)(value)
                except ValidationError as e:
                    with_error = True
                    self.errors[row_number].append({
                        'field': field,
                        'value': value,
                        'error': '{0}'.format(e)
                    })
            else:
                clean_data[field] = value
        return clean_data, with_error

    def post_clean(self, cleaned_data):
        """Extra validations"""
        return cleaned_data

    def save(self, cleaned_data):
        pass

    def post_execute(self):
        pass
