import os

from django import forms

from data_importer.models import FileUploadHistory


class FileUploadHistoryForm(forms.ModelForm):

    class Meta:
        model = FileUploadHistory
        fields = ('file_uploaded', )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.valid_extensions = ['.xlsx']
        self.fields['file_uploaded'].widget.attrs = {
            'class': 'form-control',
            'data-initial-caption': 'Upload {0} file here.'.format(', '.join(self.valid_extensions))
        }

    def clean_file_uploaded(self):
        _file = self.cleaned_data['file_uploaded']

        if not self.has_valid_extensions(_file.name):
            raise forms.ValidationError('Invalid file. Accepted files: {0}.'.format(', '.join(self.valid_extensions)))
        return _file

    def has_valid_extensions(self, file_name):
        extension = os.path.splitext(file_name)[1]
        if extension.lower() in self.valid_extensions:
            return True
        return False
