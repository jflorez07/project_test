from django.contrib import admin

from p_test.models import Person, Profile, Hobbies


class ProfileInline(admin.StackedInline):
    model = Profile


class HobbiesInline(admin.StackedInline):
    model = Hobbies
    extra = 5


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    inlines = [ProfileInline, HobbiesInline]
    list_display = ('id', 'name', 'last_name', 'email', 'deleted')
    # list_filter = ('name', 'last_name')
    search_fields = ('name', 'last_name')


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    pass


@admin.register(Hobbies)
class HobbiesAdmin(admin.ModelAdmin):
    pass
