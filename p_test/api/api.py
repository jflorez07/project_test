from rest_framework import status, generics, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from p_test.api.serializers import (
    PersonSerializer, CustomPersonSerializer, HobbiesSerializer
)
from p_test.models import Person, Hobbies


@api_view(['GET', 'POST'])
def person_api_view(request):
    if request.method == 'GET':
        persons = Person.objects.filter(deleted=False)
        person_serializer = PersonSerializer(persons, many=True)
        return Response(person_serializer.data, status=status.HTTP_200_OK)

    if request.method == 'POST':
        person_serializer = PersonSerializer(data=request.data)
        if person_serializer.is_valid():
            person_serializer.save()
            return Response({'message': 'Person Created successfully'}, status=status.HTTP_201_CREATED)
        return Response(person_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def person_detail_view(request, pk):
    _person = Person.objects.filter(id=pk, deleted=False)

    if _person.exists():
        _person = _person.latest('id')

        if request.method == 'GET':
            person_serializer = PersonSerializer(_person)
            return Response(person_serializer.data, status=status.HTTP_200_OK)

        if request.method == 'PUT':
            person_serializer = PersonSerializer(_person, data=request.data)
            if person_serializer.is_valid():
                person_serializer.save()
                return Response({'message': 'Person was updated.'}, status=status.HTTP_200_OK)
            return Response(person_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if request.method == 'DELETE':
            _person.deleted = True
            _person.save()
            return Response({'message': 'Person was deleted.'}, status=status.HTTP_200_OK)

    return Response({'message': 'Person does not exist.'}, status=status.HTTP_404_NOT_FOUND)


class HobbiesListAPIView(generics.ListCreateAPIView):
    serializer_class = HobbiesSerializer
    queryset = Hobbies.objects.all()


class PersonListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = PersonSerializer
    queryset = PersonSerializer.Meta.model.objects.filter(deleted=False)


class PersonRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PersonSerializer
    queryset = PersonSerializer.Meta.model.objects.filter(deleted=False)

    def delete(self, request, *args, **kwargs):
        person = self.queryset.filter(id=kwargs.get('pk')).first()
        if person:
            person.deleted = True
            person.save()
            return Response({'message': 'Person was deleted.'}, status=status.HTTP_200_OK)
        return Response({'message': 'Person does not exist.'}, status=status.HTTP_400_BAD_REQUEST)


class PersonViewSet(viewsets.ModelViewSet):
    serializer_class = PersonSerializer
    queryset = PersonSerializer.Meta.model.objects.filter(deleted=False)


class HobbiesViewSet(viewsets.ModelViewSet):
    serializer_class = HobbiesSerializer
    queryset = HobbiesSerializer.Meta.model.objects.all()
