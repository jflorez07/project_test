from rest_framework.routers import DefaultRouter

from p_test.api.api import PersonViewSet, HobbiesViewSet


router = DefaultRouter()

router.register(r'persons', PersonViewSet)
router.register(r'hobbies', HobbiesViewSet)

urlpatterns = router.urls
