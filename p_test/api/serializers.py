from rest_framework import serializers

from p_test.models import Person, Hobbies


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        # fields = '__all__'
        exclude = ('photo', 'deleted')

    # def to_representation(self, instance):
    #     return {
    #         'id': instance.id,
    #         'name': instance.name,
    #         'last_name': instance.last_name
    #     }


class CustomPersonSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)
    email = serializers.EmailField()

    def create(self, validated_data):
        return Person.objects.create(**validated_data)

    def update(self, instance, validated_data):
        updated = super().update(instance, validated_data)
        return updated


class HobbiesSerializer(serializers.ModelSerializer):
    # Forma 1
    # person = PersonSerializer()

    # Forma 2
    # person = serializers.StringRelatedField()

    class Meta:
        model = Hobbies
        fields = ('id', 'person', 'name')

    # Forma 3
    # def to_representation(self, instance):
    #     return {
    #         'person_id': instance.person.id,
    #         'person_name': instance.person.name,
    #         'hobby': instance.name
    #     }
