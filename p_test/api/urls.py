from django.urls import path

from p_test.api.api import (
    person_api_view, person_detail_view, HobbiesListAPIView, PersonListCreateAPIView,
    PersonRetrieveUpdateDestroyAPIView
)


urlpatterns = [
    path('persons/', person_api_view, name='person_api_view'),
    path('persons/<int:pk>/', person_detail_view, name='person_detail_api_view'),
    path('hobbies/', HobbiesListAPIView.as_view(), name='hobbies_api_view'),
    path('persons/list-create/', PersonListCreateAPIView.as_view(), name='person_list_create'),
    path('persons/retrieve-update-delete/<int:pk>/', PersonRetrieveUpdateDestroyAPIView.as_view(),
         name='person_retrieve_update_delete'),
]
