from django import forms


from p_test.models import Person


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ('name', 'last_name', 'email', 'photo')
