from django.core.exceptions import ValidationError

from data_importer.core import DataImporter
from p_test.models import Person


class PersonImporter(DataImporter):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.columns = (
            ('name', 0),
            ('last_name', 1),
            ('email', 2),
        )

    def clean_name(self, value):
        if not value:
            raise ValidationError('Name is required.')
        return value

    def save(self, cleaned_data):
        instance = Person(
            name=cleaned_data['name']
        )

        if cleaned_data['last_name']:
            instance.last_name = cleaned_data['last_name']

        if cleaned_data['email']:
            instance.email = cleaned_data['email']

        instance.save()
