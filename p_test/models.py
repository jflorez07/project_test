from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250, blank=True, null=True)
    email = models.CharField(max_length=250, blank=True, null=True)
    cell_phone = models.CharField(max_length=250, blank=True, null=True)
    photo = models.ImageField(upload_to='p_test/photos', blank=True, null=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return '{0} {1}'.format(self.name, self.last_name)

    def list_hobbies(self):
        hobbies = list(self.hobbies_set.all().values_list('name', flat=True))
        return ', '.join(hobbies)


class Profile(models.Model):
    person = models.OneToOneField(Person, on_delete=models.CASCADE)
    address = models.CharField(max_length=250, null=True, blank=True)
    job = models.CharField(max_length=250, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Profile: {0}'.format(self.person.name)


class Hobbies(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    name = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return "{0}' hobby: {1}".format(self.person.name, self.name)
