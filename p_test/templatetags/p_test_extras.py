from django import template


register = template.Library()


@register.filter(name='hobby_list')
def hobby_list(person):
    hobbies = list(person.hobbies_set.all().values_list('name', flat=True))
    return hobbies

