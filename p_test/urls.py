from django.urls import path

from p_test.views import (
    PersonCreateView, update_person, create_person, PersonListView, PersonDetailView, import_person_view
)


app_name = 'p_test'
urlpatterns = [
    path('person/list', PersonListView.as_view(), name='list'),
    path('person/detail/<int:pk>/', PersonDetailView.as_view(), name='detail'),
    path('person/<int:id>/<str:name>/update', update_person, name='update'),
    path('person/new', create_person, name='create'),
    path('person/import', import_person_view, name='import_person_view'),
]
