from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView

from data_importer.forms import FileUploadHistoryForm
from p_test.importers import PersonImporter
from p_test.models import Person

from p_test.forms import PersonForm
from students.models import Careers, Students


# Create your views here.
from students.forms import StudentsForm


def list_persons(request):
    persons = Person.objects.all().values('id', 'name')

    context = {
        'persons': persons
    }
    return render(request, 'p_test/list.html', context)


class PersonListView(ListView):
    template_name = 'p_test/list.html'
    model = Person
    context_object_name = 'persons'

    def get_queryset(self):
        persons = Person.objects.all()
        return persons


def detail_person(request, id):
    try:
        person = Person.objects.get(id=id)
        exists = True
    except Person.DoesNotExist:
        person = 'Does not exist.'
        exists = False

    context = {
        'person': person,
        'exists': exists
    }

    return render(request, 'p_test/detail.html', context)


class PersonDetailView(DetailView):
    template_name = 'p_test/detail.html'
    queryset = Person.objects.all()


def update_person(request, id, name):
    try:
        person = Person.objects.get(id=id)
        person.name = name
        person.save()
        message = 'Updated.'
    except Person.DoesNotExist:
        message = 'Not Updated.'

    context = {
        'message': message
    }

    return render(request, 'p_test/update.html', context)


def create_person(request):
    title = 'Create Student'

    if request.method == 'POST':
        form = PersonForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    else:
        form = PersonForm()

    context = {
        'form': form,
        'title': title
    }

    return render(request, 'p_test/create.html', context)


class PersonCreateView(CreateView):
    template_name = 'p_test/create.html'
    form_class = PersonForm
    success_url = reverse_lazy('p_test:list')


def import_person_view(request):
    template_name = 'data_importer/template_importer.html'
    context = {
        'title': 'Person Importer',
        'form': FileUploadHistoryForm()
    }

    if request.method == 'POST':
        form = FileUploadHistoryForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.name = 'Person Importer'
            instance.user = request.user
            instance.save()

            importer = PersonImporter(instance=instance)
            importer.execute()

            context.update({
                'form': form,
                'message': 'File imported successfully.'
            })

        else:
            context.update({'form': form})

    return render(request, template_name, context)

