from django.contrib import admin

from students.models import Students, Careers


@admin.register(Students)
class StudentsAdmin(admin.ModelAdmin):
    list_display = ('name', 'last_name', 'address', 'phone', 'careers')


@admin.register(Careers)
class CareersAdmin(admin.ModelAdmin):
    list_display = ('name', )

