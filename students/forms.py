from django import forms


from students.models import Students, Careers


class StudentsForm(forms.ModelForm):
    career = forms.ModelMultipleChoiceField(
        queryset=Careers.objects.all(),
        widget=forms.CheckboxSelectMultiple
    )

    class Meta:
        model = Students
        fields = ('name', 'last_name', 'address', 'phone', 'career')

    def clean_career(self):
        pass
