from django.db import models

from django.contrib.auth.models import User


class Careers(models.Model):
    name = models.CharField(max_length=250)

    class Meta:
        verbose_name = 'Career'
        verbose_name_plural = 'Careers'

    def __str__(self):
        return self.name


class Students(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=10)
    career = models.ManyToManyField(Careers)

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Students'

    def __str__(self):
        return '{0}: {1}'.format(self.name, self.careers())

    def careers(self):
        return ', '.join(list(self.career.all().values_list('name', flat=True)))
